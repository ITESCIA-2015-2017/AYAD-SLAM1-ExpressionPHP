package com.example.Thomas.applicationdeconversion;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }


    public void add(View v) {

        RadioButton m1 = (RadioButton) findViewById(R.id.rb1);
        RadioButton k1 = (RadioButton) findViewById(R.id.rb2);
        RadioButton mm1 = (RadioButton) findViewById(R.id.rb3);
        RadioButton f1 = (RadioButton) findViewById(R.id.rb4);
        RadioButton m2 = (RadioButton) findViewById(R.id.rb5);
        RadioButton k2 = (RadioButton) findViewById(R.id.rb6);
        RadioButton mm2 = (RadioButton) findViewById(R.id.rb7);
        RadioButton f2 = (RadioButton) findViewById(R.id.rb8);
        EditText tb = (EditText) findViewById(R.id.editText);
        TextView res = (TextView) findViewById(R.id.textView3);
        Button convert = (Button) findViewById(R.id.button);
        double a=Double.parseDouble(String.valueOf(tb.getText()));

        if ((m1.isChecked()) && (mm2.isChecked())) {
            res.setText(mtomm(a)+"");
        }
        if ((m1.isChecked()) && (f2.isChecked())) {
            res.setText(mtof(a)+"");
        }
        if ((m1.isChecked()) && (k2.isChecked()))
        {
            res.setText(mtok(a)+"");
        }
        if ((m1.isChecked()) && (m2.isChecked()))
        {
            res.setText(mtom(a)+"");
        }
        if ((k1.isChecked()) && (m2.isChecked()))
        {
            res.setText(ktom(a)+"");
        }
        if ((k1.isChecked()) && (k2.isChecked()))
        {
            res.setText(ktok(a)+"");
        }
        if ((k1.isChecked()) && (mm2.isChecked()))
        {
            res.setText(ktomm(a)+"");
        }
        if ((k1.isChecked()) && (f2.isChecked()))
        {
            res.setText(ktof(a)+"");
        }
        if ((mm1.isChecked()) && (m2.isChecked()))
        {
            res.setText(mmtom(a)+"");
        }
        if ((mm1.isChecked()) && (k2.isChecked()))
        {
            res.setText(mmtok(a)+"");
        }
        if ((mm1.isChecked()) && (mm2.isChecked()))
        {
            res.setText(mmtomm(a)+"");
        }
        if ((mm1.isChecked()) && (f2.isChecked()))
        {
            res.setText(mmtof(a)+"");
        }
        if ((f1.isChecked()) && (m2.isChecked()))
        {
            res.setText(ftom(a)+"");
        }
        if ((f1.isChecked()) && (k2.isChecked()))
        {
            res.setText(ftok(a)+"");
        }
        if ((f1.isChecked())&& (mm2.isChecked()))
        {
            res.setText(ftomm(a)+"");
        }
        if ((f1.isChecked()) && (f2.isChecked()))
        {
            res.setText(ftof(a)+"");
        }
    }

    private double mtomm(double a)
    {
        return (a*0.000621371);
    }

    private double mtof(double a)
    {
        return (a*3.28084);
    }

    private double mtom (double a)
    {
        return (a);
    }

    private double mtok (double a)
    {
        return (a*0.001);
    }

    private double ktom (double a)
    {
        return (a*1000);
    }

    private double ktok (double a)
    {
        return (a);
    }

    private double ktomm (double a)
    {
        return (a*0.621371);
    }

    private double ktof (double a)
    {
        return (a*3280.84);
    }

    private double mmtom (double a)
    {
        return (a*1609.34);
    }

    private double mmtok (double a)
    {
        return (a*1.60934);
    }

    private double mmtomm (double a)
    {
        return (a);
    }

    private double mmtof (double a)
    {
        return (a*5280);
    }

    private double ftom (double a)
    {
        return (a*0.3048);
    }

    private double ftok (double a)
    {
        return (a*0.0003048);
    }

    private double ftomm (double a)
    {
        return (a*0.000189394);
    }

    private double ftof (double a)
    {
        return (a);
    }
}